var mongoose = require('mongoose');

var PersonSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    jobTitle:String,
    organisationName:String,
    websiteUrl:String,
    country:String,
    reason:String,
    created_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Person', PersonSchema);