var express = require('express');
var router = express.Router();

var countryData = require("../data/countries.json");
/* GET users listing. */
router.get('/', function (req, res, next) {
    var cName = req.query.name;
    var matches = countryData.data.filter(function(windowValue){
        if(windowValue) {
            return windowValue.toLowerCase().indexOf(cName.toLowerCase()) >= 0;
        }
    });
    if(matches.length > 10) {
        matches.length = 10;
    }
    var objToReturn = {
        matches: matches
    };
    res.send(objToReturn);
});

module.exports = router;
