var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Person = require('../models/Person.js');

router.post("/", function(req,res, next){
    Person.create(req.body, function (err, post) {
        if (err) return next(err);
        res.json({message:"success"});
      });
});

router.get("/", function(req,res,next){
    Person.find(function (err, persons) {
        if (err) return next(err);
        res.json(persons);
    });
});

module.exports = router;